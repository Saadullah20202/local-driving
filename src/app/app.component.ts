import { Component, ViewChild } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import { ApiProvider } from '../providers/api/api';
import {ProfilePage} from '../pages/profile/profile';
import { Testhistory } from '../pages/testhistory/testhistory';
import { FeedbackPage } from '../pages/feedback/feedback';


@Component({
  templateUrl: 'app.html',
  providers : [ApiProvider]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  api_token:any;
  active:any;
  payment:any;
  pages: Array<{title: string, component: any, icon:any }>;
  constructor(platform: Platform, public service:ApiProvider) {
    platform.ready().then(() => {
      this.pages = [
        { title: 'Profile', component: ProfilePage, icon:"person" },
         { title: 'History', component: Testhistory, icon:"filing" },
         { title: 'Feedback', component: FeedbackPage, icon:"paper" }
          ];
          //  Okay, so the platform is ready and our plugins are available.
          //  Here you can do any higher level native things you might need.
                 this.api_token = localStorage.getItem('api_token');
                 this.active = localStorage.getItem('active');
                 this.payment = localStorage.getItem('payment');
                 console.log(this.payment);
                 service.getuserdata().subscribe(data => {
                   this.active = data.json().data.active;
                 },err => {
                   this.rootPage = LoginPage;
                   localStorage.removeItem('userdata');
                   localStorage.removeItem('api_token');
                   localStorage.removeItem('active');
                   localStorage.removeItem('payment');
                 });

                  if(this.api_token != null && this.active == 'true'){  //&& this.payment == 'true'
                  this.rootPage = HomePage;
                 }else{
                   this.rootPage = LoginPage ;
                 }

                  StatusBar.styleDefault();
                  Splashscreen.hide();
                });
  }

  logout(){
    this.nav.setRoot(LoginPage);
    localStorage.removeItem('active');
    localStorage.removeItem('api_token');
    localStorage.removeItem('userdata');
    localStorage.removeItem('payment');

  }

  home(){
    this.nav.setRoot(HomePage);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

}
