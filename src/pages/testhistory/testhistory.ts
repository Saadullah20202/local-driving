import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the Testhistory page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-testhistory',
  templateUrl: 'testhistory.html',
  providers : [ApiProvider]
})
export class Testhistory {

  loader:any;
  testhistorydata:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public toastCtrl:ToastController,
              public service:ApiProvider) {

                this.loader = false;
                service.gettesthistory()
                .map(res => res.json())
                .subscribe(data => {
                  this.loader = true;
                  if(data.data.length != 0){
                     this.testhistorydata = data.data;
                  }else{
                    this.presentToast('No Records Found');
                  }
                 
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Testhistory');
  }

 /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                  message: text,
                duration: 3000,
                position: 'buttom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/

}
