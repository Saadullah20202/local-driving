import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { ApiProvider } from "../../providers/api/api";
import { PunjabiquestionPage } from "../punjabiquestion/punjabiquestion";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the Selecttestpunjabi page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "page-selecttestpunjabi",
  templateUrl: "selecttestpunjabi.html",
  providers: [ApiProvider]
})
export class SelecttestpunjabiPage {
  testdata: any;
  punjabitest: any;
  punjabilanguage: any;
  loader: any;
  question: any;

  user_test_id: any;
  postdata: any;
  status: boolean;
  posturl: any = "http://espisdev3.com/localdriving/api";
  api_token: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ApiProvider,
    public event: Events,
    public http: Http
  ) {
    this.loader = false;
    this.punjabilanguage = navParams.data.language;
    service.gettest(this.punjabilanguage).subscribe(data => {
      this.punjabitest = data.json().data;
      this.loader = true;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelecttestpunjabiPage");
  }
  punjabiquestion(id, test_name) {
    this.navCtrl.push(PunjabiquestionPage, {
      test_id: id,
      language: this.punjabilanguage,
      test_name: test_name
    });
  }
}
