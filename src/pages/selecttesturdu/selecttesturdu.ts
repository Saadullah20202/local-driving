import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { ApiProvider } from "../../providers/api/api";
import { UrduquestionPage } from "../urduquestion/urduquestion";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the Selecttesturdu page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "page-selecttesturdu",
  templateUrl: "selecttesturdu.html",
  providers: [ApiProvider]
})
export class SelecttesturduPage {
  testdata: any;
  urdutest: any;
  urdulanguage: any;
  loader: any;
  question: any;

  user_test_id: any;
  postdata: any;
  status: boolean;
  posturl: any = "http://espisdev3.com/localdriving/api";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ApiProvider,
    public event: Events,
    public http: Http
  ) {
    this.loader = false;
    this.urdulanguage = navParams.data.language;
    service.gettest(this.urdulanguage).subscribe(data => {
      this.urdutest = data.json().data;
      this.loader = true;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelecttesturduPage");
  }

  urduquestion(id, test_name) {
    this.navCtrl.push(UrduquestionPage, {
      test_id: id,
      language: this.urdulanguage,
      test_name: test_name
    });
  }
}
