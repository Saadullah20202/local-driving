import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the Editprofile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class Editprofile {

  name:any;
  phone:any;
  address:any;
  city:any;
  state:any;
  editprofile:any;
  url:any;
  loader:any;
  api_token:any=localStorage.getItem('api_token');
  userdata:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public toastCtrl:ToastController,
              public http: Http,
              public event:Events) {
    this.name = navParams.data.name;
    this.phone = navParams.data.phone;
    this.address = navParams.data.address;
    this.city = navParams.data.city;
    this.state = navParams.data.state;
    this.url = 'http://espisdev3.com/localdriving/api/users/me?api_token='+this.api_token;
    this.loader = true;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Editprofile');
  }

onTypeSelectedstate(state){
  this.state = state;
}

Editprofile(){

  this.loader = false;
    this.editprofile = {
        name:this.name,
        phone:this.phone,
        address:this.address,
        city:this.city,
        state:this.state
    };

    let head = new Headers();
    head.append('Content-Type', 'application/json');

    this.http.post(this.url,this.editprofile,{headers:head})
    .subscribe(data => {
      this.loader = true;
        this.userdata = data.json().data;
         localStorage.setItem('userdata',JSON.stringify(this.userdata));  
        this.presentToast('Sucessfully Updated');
        this.event.publish('ProfilePage');
        this.navCtrl.pop();

         
    },err => {
        this.loader = true;
          console.log(err.json());
           this.presentToast('No Update Submitted');
    })

   
    console.log(this.editprofile);
}

   
   /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/

}
