import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the Feedback page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  type:any;
  personalinfo:any;
  name:any;
  email:any;
  comment;
  feedbackform:any;
  url:any;
  loader:any;
  feedbacktype;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl:ToastController,
              public http:Http) {
                this.personalinfo = JSON.parse(localStorage.getItem('userdata'));
                this.name = this.personalinfo.name;
                this.email = this.personalinfo.email;
                this.url = 'http://espisdev3.com/localdriving/api/feedback';
               this.loader = true;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Feedback');
  }
  onTypeSelectedtype(type){

    this.type=type;

  }

  send(){
    if(this.type != null && this.comment != null){
      this.loader = false;
       this.feedbackform = {
      name:this.name,
      email:this.email,
      type:this.type,
      subject:this.comment
    }
    let head = new Headers();
    head.append('Content-Type', 'application/json');

    this.http.post(this.url,this.feedbackform,{headers:head}).subscribe(data => {

             this.presentToast(data.json().message.message);
             this.loader = true;
             this.type='';
             this.comment = '';
             this.feedbacktype = '';
    },err =>{
        this.presentToast('Email not send');
    });

    
    }else{
      this.presentToast('Please Fill the Required Feilds');
    }
   
    console.log(this.feedbackform);
   
  }

     
   /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/


}
