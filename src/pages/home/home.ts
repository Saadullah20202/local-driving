import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {SelecttestPage} from '../selecttest/selecttest';
import {SelecttesturduPage} from '../selecttesturdu/selecttesturdu';
import {SelecttesthindiPage} from '../selecttesthindi/selecttesthindi';
import {SelecttestpunjabiPage} from '../selecttestpunjabi/selecttestpunjabi';
import {ApiProvider} from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoginPage } from '../login/login';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[ApiProvider]
})
export class HomePage {
  languages;
  languageselect:any;

//////////////////////////////////////////////////////////////
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public service: ApiProvider,
              public storage: Storage,
              public http:Http) {
                this.languageselect = true;
                    console.log('homepage');

              }


  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  onTypeSelectedlanguage(value){

  }
  logout(){
    this.navCtrl.setRoot(LoginPage);
    localStorage.removeItem('active');
    localStorage.removeItem('api_token');
    localStorage.removeItem('userdata');
    localStorage.removeItem('payment');

  }

 paypall(){

   if(this.languages != null){

        if(this.languages == 'en'){
        this.navCtrl.push(SelecttestPage,{
            language : this.languages
        });
        }else if(this.languages == 'ur'){
        this.navCtrl.push(SelecttesturduPage,{
            language : this.languages
        });
        }else if(this.languages == 'hi'){
              this.navCtrl.push(SelecttesthindiPage,{
            language : this.languages
        });
        }else if(this.languages == 'pa'){
              this.navCtrl.push(SelecttestpunjabiPage,{
            language : this.languages
        });
        }

  }
else{
      this.languageselect = false;
        }


 }

close(){
  this.languageselect = true;
}

}
