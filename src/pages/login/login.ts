import { Component } from '@angular/core';
import { NavController, NavParams, ToastController,MenuController } from 'ionic-angular';
import {HomePage} from '../home/home';
import {RegistrationPage} from '../registration/registration';
import {ForgetpasswordPage} from '../forgetpassword/forgetpassword';
import { Http, Headers, RequestOptions } from '@angular/http';
import { PaymentsPage } from '../payments/payments';
import 'rxjs/add/operator/map';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  user_email:any;
  user_password:any;
  url:any = 'https://dmvtest.localdriving.com/api/login?includes=user';
  logindata:any;
  loader:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
  public toastCtrl:ToastController,
  public http:Http,
  public menu:MenuController) {
    menu.swipeEnable(false);
    this.loader = true;


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

   loginForm(){
     this.loader = false;
     var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
      if(this.user_email != '' && this.user_email !=null && re.test(this.user_email)){
        let head = new Headers();
          head.append('Content-Type', 'application/json');
          this.logindata = {email:this.user_email,
                            password:this.user_password};
          this.http.post(this.url,this.logindata,{headers:head})
          .map(res => res.json())
          .subscribe(data => {
            console.log(data);
              localStorage.setItem('api_token',data.data.api_token);
              localStorage.setItem('active',data.data.user.data.active);
              localStorage.setItem('payment',data.data.user.data.subscription);
              this.loader = true;
              //if(data.data.user.data.subscription == true){
                  this.navCtrl.setRoot(HomePage);
              //}
              /*else{
                this.navCtrl.push(PaymentsPage);
              }*/

          },err => {
            this.loader = true;
            if(err.json().error.http_code == 403){
              this.presentToast('Your Account is Inactive');
            }else if(err.json().error.http_code == 404){
              this.presentToast('Invalid Credentails');
            }else
            {
             this.presentToast(err.json().error.message);
            }
          })
        //this.navCtrl.push(HomePage);
      }else{
        this.loader = true;
        this.presentToast('enter valid email')
      }

  }

forgetpass(){
 this.navCtrl.push(ForgetpasswordPage);
}

registernow(){
  console.log('register now');
  this.navCtrl.push(RegistrationPage);
}


    /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/
}
