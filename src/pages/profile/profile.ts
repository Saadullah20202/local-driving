import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Editprofile } from '../editprofile/editprofile';

/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

personalinfo:any;
name:any;
phone:any;
address:any;
email:any;
city:any;
state:any;
subscription:any;
subscription_date:any;
subscription_expiry:any;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public event:Events) {


                event.subscribe('ProfilePage',() => {

                  this.personalinfo = JSON.parse(localStorage.getItem('userdata'));
                  console.log(this.personalinfo);
                  this.name = this.personalinfo.name;
                  this.phone = this.personalinfo.phone;
                  this.email = this.personalinfo.email;
                  this.address = this.personalinfo.address;
                  this.city = this.personalinfo.city;
                  this.state = this.personalinfo.state;
                  this.subscription_date = this.personalinfo.registeration_date;
                  this.subscription_expiry = this.personalinfo.subscription_expiry; 

                });





    this.personalinfo = JSON.parse(localStorage.getItem('userdata'));
    console.log(this.personalinfo);
    this.name = this.personalinfo.name;
    this.phone = this.personalinfo.phone;
    this.email = this.personalinfo.email;
    this.address = this.personalinfo.address;
    this.city = this.personalinfo.city;
    this.state = this.personalinfo.state;
    this.subscription_date = this.personalinfo.registeration_date;
    this.subscription_expiry = this.personalinfo.subscription_expiry; 

    if(this.personalinfo.subscription == true){
      this.subscription = 'Active';
    }else{
      this.subscription = 'Inactive';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }

  editprofile(name,phone,email,address,city,state){

    this.navCtrl.push(Editprofile,{
      name:name,
      phone:phone,
      email:email,
      address:address,
      city:city,
      state:state
    });

  }

}
