import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
  Events,
  Navbar
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { ApiProvider } from "../../providers/api/api";
import { Media, MediaObject } from "@ionic-native/media";
import { ThanksPage } from "../thanks/thanks";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
/*
  Generated class for the Urduquestion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: "page-urduquestion",
  templateUrl: "urduquestion.html",
  providers: [ApiProvider]
})
export class UrduquestionPage {
  @ViewChild(Navbar) navBar: Navbar;
  playmusic: any;
  stopmusic: any;
  test_id: any;
  urdulanguage: any;
  urdutestdata: any;
  questionarray: any;
  question: any;
  index: any = 0;
  noquestions: any;
  questions: any;
  loader: any;
  answers: any;
  imagehidden: any;
  question_image: any;
  musicfile: any;
  file: any;
  playaudio: boolean = false;
  counter: number = 1;
  totalquestions: any;
  test_name: any;
  answer: any;
  answer1: any = null;
  postdata: any;
  new_array: any;
  new_array_element: any;
  answerselect: any;
  savetest: any;
  status: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public storage: Storage,

    public service: ApiProvider,
    private media: Media,
    public event: Events,
    public http: Http
  ) {
    this.playmusic = false;
    this.stopmusic = true;
    this.test_id = navParams.data.test_id;
    this.test_name = navParams.data.test_name;
    this.urdulanguage = navParams.data.language;
    this.questionarray = [];
    this.questions = true;
    this.noquestions = true;
    this.loader = false;
    this.imagehidden = true;
    this.answerselect = true;
    this.status = false;
    this.savetest = true;

    service.gettestquestions(this.test_id).subscribe(data => {
      this.urdutestdata = data.json().data;
      if (this.urdutestdata != null && this.urdutestdata.length != 0) {
        localStorage.setItem("user_test_id", this.test_id);
        localStorage.setItem("test_status", JSON.stringify(this.status));

        this.totalquestions = this.urdutestdata.length;
        this.questions = false;
        this.loader = true;
        for (let i = 0; i < this.urdutestdata.length; i++) {
          this.questionarray[i] = this.urdutestdata[i];
          this.question = this.questionarray[0].question;
          if (this.questionarray[0].question_image != null) {
            this.imagehidden = false;
            this.question_image = this.questionarray[0].question_image;
          } else {
            this.imagehidden = true;
          }

          if (
            this.questionarray[0].answers.data != null &&
            this.questionarray[0].answers.data.length != 0
          ) {
            this.answers = this.questionarray[0].answers.data;
          } else {
            this.presentToast("جواب نہیں ملا");
          }
          if (this.questionarray[0].question_audio != null) {
            this.musicfile =
              this.service.musicurl + this.questionarray[0].question_audio;
          } else {
            this.musicfile = null;
          }
        }
      } else {
        this.noquestions = false;
        this.loader = true;
      }
    });
  }

  ionViewDidLoad() {}

  radioClicked(value, i) {
    this.answer = value;
    this.answer1 = value;
    let temp_id = "abc-" + i;
    let element = document.getElementById(temp_id);
    let body = document.getElementsByTagName("ion-item");
    for (let x = 0; x < body.length; x++) {
      body[x].classList.remove("correctanswer");
      body[x].classList.remove("wronganswer");
    }

    if (value == true) {
      element.classList.add("correctanswer"); //add the class
    } else {
      element.classList.add("wronganswer");
    }
  }

  audioplay(musicfile) {
    if (musicfile != null) {
      this.file = this.media.create(musicfile);
      this.presentToast("لوڈ ہو رہا ہے انتظار کرے");
      this.file.play();
      this.playaudio = true;
    } else {
      this.presentToast("آڈیو نہیں ملا");
    }
  }

  nextquestion(test_id) {
    if (this.playaudio) {
      this.file.stop();
    }

    if (this.answer1 != null) {
      if (this.answer == true) {
        this.question_image = null;
        this.answer = false;
        if (this.index == this.questionarray.length - 1) {
          console.log("hide button");
          this.navCtrl.setRoot(ThanksPage, {
            language: this.urdulanguage,
            test_id: test_id
          });
        } else {
          this.counter = this.counter + 1;
          this.index = this.index + 1;
          this.question = this.questionarray[this.index].question;
          if (this.questionarray[this.index].question_image != null) {
            this.imagehidden = false;
            this.question_image = this.questionarray[this.index].question_image;
          } else {
            this.imagehidden = true;
          }
          if (
            this.questionarray[this.index].answers.data != null &&
            this.questionarray[this.index].answers.data.length != 0
          ) {
            this.answers = this.questionarray[this.index].answers.data;
          } else {
            this.presentToast("جواب نہیں ملا");
          }
          if (this.questionarray[this.index].question_audio != null) {
            this.musicfile =
              this.service.musicurl +
              this.questionarray[this.index].question_audio;
          } else {
            this.musicfile = null;
          }
        }
      } else {
        this.answerselect = false;
      }
      console.log("next question");
    } else {
      this.answerselect = false;
    }
  }
  close() {
    this.answerselect = true;
  }

  posttest() {
    this.event.publish("SelecttesturduPage");
    this.navCtrl.pop();
  }

  staypage() {
    this.savetest = true;
  }

  previousquestion() {
    if (this.playaudio) {
      this.file.stop();
    }
    if (this.index == 0) {
      console.log("hidebutton");
    } else {
      this.question_image = null;
      this.counter = this.counter - 1;
      this.index = this.index - 1;
      this.question = this.questionarray[this.index].question;
      if (this.questionarray[this.index].question_image != null) {
        this.imagehidden = false;
        this.question_image = this.questionarray[this.index].question_image;
      } else {
        this.imagehidden = true;
      }
      if (this.questionarray[this.index].answers.data != null) {
        this.answers = this.questionarray[this.index].answers.data;
      } else {
        this.presentToast("جواب نہیں ملا");
      }
      if (this.questionarray[this.index].question_audio != null) {
        this.musicfile =
          this.service.musicurl + this.questionarray[this.index].question_audio;
      } else {
        this.musicfile = null;
      }
    }
    console.log("previous question");
  }

  /* ---------------------------- Toast Function ---------------------------------------*/
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "buttom"
    });
    toast.present();
  }
  /* ----------------------------END Toast Function ---------------------------------------*/
}
