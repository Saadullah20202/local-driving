import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';


/*
  Generated class for the Forgetpassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html'
})
export class ForgetpasswordPage {
  user_email:any;
  email:any;
  url:any = 'http://espisdev3.com/localdriving/api/password/email';
  loader:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public http:Http,
              public toastCtrl:ToastController) {
                this.loader = true;
              }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpasswordPage');
  }

  getpassword(){
    this.loader = false;
    this.email = {email:this.user_email};
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
      if(this.user_email != '' && this.user_email !=null && re.test(this.user_email)){
             let head = new Headers();
             head.append('Content-Type', 'application/json');
              console.log(this.email);  
              
              // POST REQUEST
              this.http.post(this.url,this.email,{headers:head})
              .subscribe(data => {

              if(data.json().message.message == 'Email sent.'){
                  this.presentToast(data.json().message.message);
                  this.navCtrl.pop();
              }else{
                this.presentToast(data.json().message.message);
              }
              
              this.loader = true;
             
            },err => {
              this.loader = true;
              console.log(err);
            });
    }else{
        this.presentToast('Please Enter valid Email');
    }

  }

  /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/
}
