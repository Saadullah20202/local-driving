import { Component } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
//import { LoginPage } from '../login/login';
import {HomePage} from '../home/home';
//import { PaymentsPage } from '../payments/payments';

/*
  Generated class for the Registration page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {
  user_full_name:any;
  user_email:any;
  user_password:any;
  user_phone_number:any;
  user_address:any;
  user_city:any;
  user_state:any;
  loader:any;
  payment:boolean = false;
  userdata:any;
  amount:any = 10;
  user_id:any;
  api_token:any;
  status:any;

  url:any = 'https://dmvtest.localdriving.com/api/register?includes=user';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl:ToastController,
              public http:Http) {
                  this.loader = true;
                  this.user_state = 'Alabama';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }


  registration(){



    this.loader = false;
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
      if(this.user_email != '' && this.user_email !=null && re.test(this.user_email)){

        let head = new Headers();
          head.append('Content-Type', 'application/json');
    this.userdata = {
      name: this.user_full_name,
      email: this.user_email,
      password: this.user_password,
      phone: this.user_phone_number,
      address: this.user_address,
      city: this.user_city,
      state: this.user_state ,
      payment:this.payment
    }

    this.http.post(this.url,this.userdata,{headers:head})
    .map(res => res.json())
    .subscribe(data => {

           this.user_id = data.data.user.data.id;
           console.log(data.data);
           localStorage.setItem('api_token',data.data.api_token);
           localStorage.setItem('active',data.data.user.data.active);
           this.loader = true;
           this.presentToast('Sucessfully Registered');
           this.navCtrl.setRoot(HomePage);
         /*
           this.navCtrl.push(PaymentsPage,{
             id:this.user_id
           });
           */
    },err => {
      for(let error of err.json().errors){
        this.presentToast(error.message);
        this.loader = true;
      }
    });
  }else{
    this.presentToast('Enter Valid Email');
  }
  }

   /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/
}
