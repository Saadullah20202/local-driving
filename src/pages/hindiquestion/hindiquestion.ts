import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, Events, Navbar} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Media, MediaObject } from '@ionic-native/media';
import { ThanksPage } from '../thanks/thanks';
import 'rxjs/add/operator/map';

/*
  Generated class for the Hindiquestion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-hindiquestion',
  templateUrl: 'hindiquestion.html',
  providers : [ApiProvider]
})


export class HindiquestionPage {
  @ViewChild(Navbar) navBar: Navbar;

 playmusic:any;
 stopmusic:any;
 hindilanguage:any;
 test_id:any;
 hinditestdata:any;
 question:any;
 questionarray:any;
 index:any = 0;
 noquestions:any;
 questions:any;
 loader:any;
 imagehidden:any;
 question_image:any;
 answers:any;
 file:MediaObject;
 musicfile:any;
 playaudio:boolean = false;
 counter:number = 1;
 totalquestions:any;
 answer:any;
 answer1:any = null;

  api_token:any;
  postdata:any;
  new_array:any;
  new_array_element:any;
  answerselect:any;
  savetest:any;
  status:boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl:ToastController,
              public media:Media,
              public service : ApiProvider,
              public event:Events) {
                this.playmusic = false;
                this.stopmusic = true;
                this.api_token = localStorage.getItem('api_token');
                this.test_id = navParams.data.test_id;
                this.hindilanguage = navParams.data.language;
                console.log(this.hindilanguage);
                this.questionarray = [];
                this.questions = true;
                this.noquestions = true;
                this.loader = false;
                this.answerselect = true;
                this.savetest = true;
                this.status = false;

                service.gettestquestions(this.test_id).subscribe(data => {
                    this.hinditestdata = data.json().data;

                    if(this.hinditestdata != null && this.hinditestdata.length != 0){

                      localStorage.setItem('user_test_id',this.test_id);
                      localStorage.setItem('test_status',JSON.stringify(this.status));

                      this.totalquestions = this.hinditestdata.length;
                      this.questions = false;
                      this.loader = true;
                      for(let i=0; i < this.hinditestdata.length;i++){
                         this.questionarray[i] = this.hinditestdata[i];
                         this.question = this.questionarray[0].question;
                          if(this.questionarray[0].question_image != null){
                      this.imagehidden = false;
                      this.question_image = this.questionarray[0].question_image;
                    }else{
                      this.imagehidden = true;
                    }

                    if(this.questionarray[0].answers.data != null && this.questionarray[0].answers.data.length != 0){
                      this.answers = this.questionarray[0].answers.data;
                    }else{
                      this.presentToast('उत्तर नहीं मिला');
                    }
                    if(this.questionarray[0].question_audio != null){
                      this.musicfile =  this.service.musicurl+this.questionarray[0].question_audio;
                    }else{
                      this.musicfile = null;
                    }
                      }
                    }else{
                      this.loader = true;
                       this.noquestions = false;
                    }

                });

              }

  ionViewDidLoad() {
  }

  radioClicked(value,i){
     this.answer = value;
     this.answer1 = value;
    let temp_id = 'abc-'+i;
      let element = document.getElementById(temp_id);
       let body = document.getElementsByTagName('ion-item');
       for(let x=0;x<body.length;x++) {
      body[x].classList.remove('correctanswer');
       body[x].classList.remove('wronganswer');

       }

         if (value == true){

      element.classList.add('correctanswer');   //add the class

    }else{
       element.classList.add('wronganswer');

    }
  }

  audioplay(musicfile){
    if(musicfile != null){
         this.file = this.media.create(musicfile);
         this.presentToast('कलोड हो रहा है, कृपया प्रतीक्षा करें ');
         this.file.play();
         this.playaudio = true;
  }else{
    this.presentToast('ऑडियो नहीं मिला');
  }

  }

  nextquestion(test_id){
    if(this.playaudio){
      this.file.stop();
    }

if(this.answer1 != null){
  this.answer1 = null;
    if(this.answer == true){
      this.question_image = null;
       this.answer = false;

    if(this.index == this.questionarray.length-1){
      console.log('hide button');
       this.navCtrl.setRoot(ThanksPage,{
        language: this.hindilanguage,
          test_id:test_id
        });
    }else{

        this.counter = this.counter+1;
        this.index = this.index+1;
        this.question = this.questionarray[this.index].question;
         if(this.questionarray[this.index].question_image != null){
                      this.imagehidden = false;
                      this.question_image = this.questionarray[this.index].question_image;
                    }else{
                      this.imagehidden = true;
                    }
        if(this.questionarray[this.index].answers.data != null && this.questionarray[this.index].answers.data.length != 0){
             this.answers = this.questionarray[this.index].answers.data;
        }else{
          this.presentToast('उत्तर नहीं मिला');
        }
     if(this.questionarray[this.index].question_audio != null){
         this.musicfile =  this.service.musicurl+this.questionarray[this.index].question_audio;
       }else{
         this.musicfile = null;
       }

    }
    }else{
      this.answerselect = false;
  }
    console.log('next question');

  }else{
      this.answerselect = false;
  }
}

close(){
  this.answerselect = true;
}

      posttest(){
       this.event.publish('SelecttesthindiPage');
       this.navCtrl.pop();
     }

     staypage(){
       this.savetest = true;
     }


  previousquestion(){
    if(this.playaudio){
      this.file.stop();
    }
    if(this.index == 0){
      console.log('hidebutton');
    }else{
      this.question_image = null;
        this.counter = this.counter - 1;
      this.index = this.index-1;
      this.question = this.questionarray[this.index].question;
       if(this.questionarray[this.index].question_image != null){
                      this.imagehidden = false;
                      this.question_image = this.questionarray[this.index].question_image;
                    }else{
                      this.imagehidden = true;
                    }
         if(this.questionarray[this.index].answers.data != null){
             this.answers = this.questionarray[this.index].answers.data;
        }else{
          this.presentToast('उत्तर नहीं मिला');
        }
     if(this.questionarray[this.index].question_audio != null){
         this.musicfile =  this.service.musicurl+this.questionarray[this.index].question_audio;
       }else{
         this.musicfile = null;
       }
    }
    console.log('previous question');
  }


        /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'buttom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/
}
