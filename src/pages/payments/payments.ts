import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, MenuController } from 'ionic-angular';
import { QuestionPage } from '../question/question';
import { UrduquestionPage } from '../urduquestion/urduquestion';
import { HindiquestionPage } from '../hindiquestion/hindiquestion';
import { PunjabiquestionPage } from '../punjabiquestion/punjabiquestion';
import { HomePage } from '../home/home';
import { ApiProvider } from '../../providers/api/api';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the Payments page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
  providers : [ApiProvider]
})
export class PaymentsPage {

user_id:any;
cardnumber:any;
expirydate:any;
cardcode:any;
payment:any;
api_token:any;
testkey:any;
charges:any;
duration:any;
term_popup:any;
min: string = '';
max: string = '';
checkboxvalue:any

posturl:any = 'http://espisdev3.com/localdriving/api';

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public toastCtrl:ToastController,
              public menu:MenuController,
              public http:Http,
              public service: ApiProvider) {
                this.term_popup = true;
                  let year = new Date();
                  year.setDate(year.getFullYear());
                  console.log(year);

                  this.max = year.toISOString();


    
    menu.swipeEnable(false);

    service.getpakage().subscribe(data => {

      console.log(data.json().data);
      this.charges = data.json().data.subscription_charges;
      this.duration = data.json().data.subscription_expiry_time_months;

    });

    this.api_token = localStorage.getItem('api_token');
    this.payment = localStorage.getItem('payment');
    console.log(this.payment);
    this.user_id = navParams.data.id;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Payments');
  }

paymentsubmit(){
  console.log(this.cardnumber);
  console.log(this.cardcode);
  console.log(this.expirydate);
  
 if(this.cardnumber != null &&
     this.cardcode != null &&
     this.expirydate != null){
       if(this.checkboxvalue == true){
       this.payment = true;
       if(this.payment == true){
         this.http.post(this.posturl+'/users/me/subscriptions?api_token='+this.api_token,this.testkey).subscribe(data => {
                     localStorage.setItem('payment',this.payment);
                      this.navCtrl.setRoot(HomePage);
         });

       }
       else{
         this.presentToast('Please Pay for futher Proceeding');
       }

   }else{
         this.presentToast('Please accept Terms and Conditions for further Proceeding');  
   }
  }else{
   
    this.presentToast('Please Fill All Required Feilds');
  }
  
}
 /* ---------------------------- Toast Function ---------------------------------------*/
    private presentToast(text) {
          let toast = this.toastCtrl.create({
                message: text,
                duration: 3000,
                position: 'bottom'
           });
    toast.present();
    }
     /* ----------------------------END Toast Function ---------------------------------------*/

     close(){
       this.term_popup = true;
     }

     openpopup(){
       this.term_popup = false;
     }

     updateCucumber(){
       console.log(this.checkboxvalue);
     }
}
