import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { QuestionPage } from "../question/question";
import { ApiProvider } from "../../providers/api/api";
import { Http, Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the Selecttest page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "page-selecttest",
  templateUrl: "selecttest.html",
  providers: [ApiProvider]
})
export class SelecttestPage {
  englishtest: any;
  englishlanguage: any;
  loader: any;
  question: any;
  test_id: any;
  test_name: any;
  user_test_id: any;
  postdata: any;
  status: boolean;
  posturl: any = "http://espisdev3.com/localdriving/api";
  api_token: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ApiProvider,
    public event: Events,
    public http: Http
  ) {
    this.loader = false;
    this.englishlanguage = navParams.data.language;
    service.gettest(this.englishlanguage).subscribe(data => {
      this.englishtest = data.json().data;
      console.log(this.englishtest);
      this.loader = true;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelecttestPage");
  }

  quesions(test_id, test_name) {
    console.log(test_id);
    this.navCtrl.push(QuestionPage, {
      test_id: test_id,
      language: this.englishlanguage,
      test_name: test_name
    });
  }
}
