import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { HindiquestionPage } from "../hindiquestion/hindiquestion";
import { ApiProvider } from "../../providers/api/api";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the Selecttesthindi page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "page-selecttesthindi",
  templateUrl: "selecttesthindi.html",
  providers: [ApiProvider]
})
export class SelecttesthindiPage {
  testdata: any;
  hinditest: any;
  hindilanguage: any;
  loader: any;
  question: any;

  user_test_id: any;
  postdata: any;
  status: boolean;
  posturl: any = "http://espisdev3.com/localdriving/api";
  api_token: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ApiProvider,
    public http: Http,
    public event: Events
  ) {
    this.loader = false;
    this.hindilanguage = navParams.data.language;
    service.gettest(this.hindilanguage).subscribe(data => {
      this.hinditest = data.json().data;
      this.loader = true;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelecttesthindiPage");
  }

  hindiquestion(id, test_name) {
    this.navCtrl.push(HindiquestionPage, {
      test_id: id,
      language: this.hindilanguage,
      test_name: test_name
    });
  }
}
