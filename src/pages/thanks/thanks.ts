import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { HomePage } from "../home/home";
import { ApiProvider } from "../../providers/api/api";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the Thanks page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "page-thanks",
  templateUrl: "thanks.html",
  providers: [ApiProvider]
})
export class ThanksPage {
  test_id: any;
  language: any;
  englishthanks: any;
  urduthanks: any;
  hindithanks: any;
  punjabithanks: any;
  status: boolean = true;
  postdata: any;
  api_token: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ApiProvider,
    public http: Http
  ) {
    let head = new Headers();
    head.append("Content-Type", "application/json");
    this.api_token = localStorage.getItem("api_token");
    this.language = navParams.data.language;
    this.test_id = navParams.data.test_id;
    this.englishthanks = true;
    this.urduthanks = true;
    this.hindithanks = true;
    this.punjabithanks = true;
    this.postdata = {
      completed: this.status
    };

    if (this.language == "en") {
      this.englishthanks = false;
      this.urduthanks = true;
      this.hindithanks = true;
      this.punjabithanks = true;
      localStorage.removeItem("test_id");
      localStorage.removeItem("status");
    } else if (this.language == "ur") {
      this.englishthanks = true;
      this.urduthanks = false;
      this.hindithanks = true;
      this.punjabithanks = true;
      localStorage.removeItem("test_id");
      localStorage.removeItem("status");
    } else if (this.language == "hi") {
      this.englishthanks = true;
      this.urduthanks = true;
      this.hindithanks = false;
      this.punjabithanks = true;
      localStorage.removeItem("test_id");
      localStorage.removeItem("status");
    } else if (this.language == "pa") {
      this.englishthanks = true;
      this.urduthanks = true;
      this.hindithanks = true;
      this.punjabithanks = false;
      localStorage.removeItem("test_id");
      localStorage.removeItem("status");
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ThanksPage");
  }

  back() {
    this.navCtrl.setRoot(HomePage);
  }
  homepage() {
    this.navCtrl.setRoot(HomePage);
  }
}
